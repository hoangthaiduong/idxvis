This tool can be used to visualize 2D data in the IDX hierarchical format.

You will need Processing (https://processing.org/download/) and the ControlP5 library (open Processing, navigate to "Sketch" -> "Import Library" -> "Add library" to install).

After Processing and ControlP5 are installed, just use the Processing IDE to open the code in IdxVis.pde and run it.