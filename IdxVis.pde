// TODO: change the selected sliders to ranges

import controlP5.*;

class Vector
{
  long x;
  long y;
  Vector(PVector p)
  {
    x = (long)p.x;
    y = (long)p.y;
  }
  Vector(long x_, long y_)
  {
    x = x_;
    y = y_;
  }
  @Override
  public boolean equals(Object object)
  {
    boolean same = false;
    if (object != null && object instanceof Vector)
    {
      Vector other = (Vector)object;
      same = ((this.x == other.x) && (this.y == other.y));
    }
    return same;
  }
}

PGraphics pg;
ControlP5 cp5_;
String bit_string_ = "101010";
PVector dims_ = new PVector(8, 8);
int real_dims_x_ = 8;
int real_dims_y_ = 8;
Textfield dims_x_txt_;
Textfield dims_y_txt_;
Textfield bit_string_txt_;
final int grid_size = 40;
PFont font1_;
PFont font2_;
PFont font3_;
PFont font4_;
ArrayList<Toggle> level_toggles_ = new ArrayList<Toggle>(16);
int num_levels = bit_string_.length() + 1;
int bits_per_block_ = 2;
Textfield bits_per_block_txt_;
Textfield blocks_per_file_txt_;
int samples_per_block_ = (int)pow(2, 3);
long[] xy_to_hz_;
int[] xy_to_level_;
Slider block_slider_;
Slider file_slider_;
int selected_block_ = 0;
int partition_x_ = 4;
int partition_y_ = 4;
int process_x_ = 2;
int process_y_ = 2;
Textfield partition_x_txt_;
Textfield partition_y_txt_;
Textfield process_x_txt_;
Textfield process_y_txt_;
int blocks_per_file_ = 4;
int selected_file_ = 0;
ArrayList<Vector> selected_samples_ = new ArrayList<Vector>(64);
Toggle draw_lines_;
Toggle show_processes_;
int scale_ = 1;

Vector top_left_grid_ = new Vector(100, 250);
color[] palette_ = {#898CFF, #FF89B5, #F599C2, #888D89, #6ED0F7, #F5A26F, #668DE5, #ED6D79, #BD7DC3, #58B858};

PVector dims_from_bit_string(String bit_string)
{
  PVector dims = new PVector(0, 0, 0);
  for (int i = 0; i < bit_string.length(); ++i) {
    if (bit_string.charAt(i) == '0') {
      ++dims.x;
    }
    else if (bit_string.charAt(i) == '1') {
      ++dims.y;
    }
    else {
      throw new RuntimeException("Wrong bit string");
    }
  }
  dims.x = pow(2, dims.x);
  dims.y = pow(2, dims.y);
  return dims;
}

void setup()
{
  smooth();
  size(1440, 940);
  pg = createGraphics(2880, 1880);
  font1_ = createFont("Consolas", 48);
  font2_ = createFont("DejaVu Sans Mono", 20);
  font3_ = createFont("Consolas", 96);
  font4_ = createFont("DejaVu Sans Mono", 96);
  cp5_ = new ControlP5(this);
  PFont font = createFont("Consolas", 24);
  bit_string_txt_ = cp5_.addTextfield("bit_string")
     .setPosition(200, 20)
     .setSize(200, 24)
     .setFont(font)
     .setFocus(true)
     .setAutoClear(false)
     .setText(bit_string_)
     .setCaptionLabel("")
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          bit_string_ = bit_string_txt_.getText();
          dims_ = dims_from_bit_string(bit_string_);
          real_dims_x_ = (int)dims_.x;
          real_dims_y_ = (int)dims_.y;
          dims_x_txt_.setText("" + real_dims_x_);
          dims_y_txt_.setText("" + real_dims_y_);
          calculate_index_map(dims_, bit_string_);
          update_level_toggles();
          update_block_sliders();
          update_file_sliders();
        }
      });

   dims_x_txt_ = cp5_.addTextfield("dims x")
     .setLabelVisible(false)
     .setPosition(290, 80)
     .setSize(30, 24)
     .setFont(font)
     .setAutoClear(false)
     .setText("" + real_dims_x_)
     .setCaptionLabel("")
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          real_dims_x_ = Integer.parseInt(dims_x_txt_.getText());
        }
      });

  dims_y_txt_ = cp5_.addTextfield("dims y")
     .setLabelVisible(false)
     .setPosition(350, 80)
     .setSize(30, 24)
     .setFont(font)
     .setAutoClear(false)
     .setText("" + real_dims_y_)
     .setCaptionLabel("")
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          real_dims_y_ = Integer.parseInt(dims_y_txt_.getText());
        }
      });

  bits_per_block_txt_ = cp5_.addTextfield("bits per block")
     .setLabelVisible(false)
     .setPosition(700, 20)
     .setSize(30, 24)
     .setFont(font)
     .setAutoClear(false)
     .setText("3")
     .setCaptionLabel("")
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          bits_per_block_ = Integer.parseInt(bits_per_block_txt_.getText());
          samples_per_block_ = (int)Math.pow(2, bits_per_block_);
          update_block_sliders();
          update_file_sliders();
        }
      });
      
   blocks_per_file_txt_ = cp5_.addTextfield("blocks per file")
   .setLabelVisible(false)
   .setPosition(720, 110)
   .setSize(30, 24)
   .setFont(font)
   .setAutoClear(false)
   .setText("4")
   .setCaptionLabel("")
   .onChange(new CallbackListener() {
      public void controlEvent(CallbackEvent theEvent) {
        blocks_per_file_ = Integer.parseInt(blocks_per_file_txt_.getText());
        update_file_sliders();
      }
    });  
      
   calculate_index_map(dims_, bit_string_);
   for (int i = 0; i < bit_string_.length() + 1; ++i) {
     level_toggles_.add(cp5_.addToggle("" + i)
       .setPosition(140 + i * 22, 55)
       .setSize(20,20)
       .setValue(true)
       .setLabelVisible(false));
   }
   
   block_slider_ = cp5_.addSlider("sliderTicks2")
     .setPosition(700, 55)
     .setWidth(600)
     .setHeight(20)
     .setRange(-1, 30) // values can range from big to small as well
     .setValue(-1)
     .setNumberOfTickMarks(32)
     .setSliderMode(Slider.FLEXIBLE)
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          selected_block_ = (int)block_slider_.getValue();    
        }
      });
   
   file_slider_ = cp5_.addSlider("file slider")
     .setPosition(700, 145)
     .setWidth(600)
     .setHeight(20)
     .setRange(-1, 30) // values can range from big to small as well
     .setValue(-1)
     .setNumberOfTickMarks(32)
     .setSliderMode(Slider.FLEXIBLE)
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          selected_file_ = (int)file_slider_.getValue();    
        }
      });      
   partition_x_txt_ = cp5_.addTextfield("partition x")
     .setLabelVisible(false)
     .setPosition(700, 80)
     .setSize(30, 24)
     .setFont(font)
     .setAutoClear(false)
     .setText("" + partition_x_)
     .setCaptionLabel("")
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          partition_x_ = Integer.parseInt(partition_x_txt_.getText());          
        }
      });
   
   partition_y_txt_ = cp5_.addTextfield("partition y")
     .setLabelVisible(false)
     .setPosition(760, 80)
     .setSize(30, 24)
     .setFont(font)
     .setAutoClear(false)
     .setText("" + partition_y_)
     .setCaptionLabel("")
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          partition_y_ = Integer.parseInt(partition_y_txt_.getText());          
        }
      });
   
   show_processes_ = cp5_.addToggle("Show processes")
     .setPosition(1250, 80)
     .setSize(50,20)
     .setValue(false)
     .setColorLabel(1)
     .setMode(ControlP5.SWITCH);
   
   process_x_txt_ = cp5_.addTextfield("process x")
     .setLabelVisible(false)
     .setPosition(1120, 80)
     .setSize(30, 24)
     .setFont(font)
     .setAutoClear(false)
     .setText("" + process_x_)
     .setCaptionLabel("")
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          process_x_ = Integer.parseInt(process_x_txt_.getText());          
        }
      });
   
   process_y_txt_ = cp5_.addTextfield("process y")
     .setLabelVisible(false)
     .setPosition(1180, 80)
     .setSize(30, 24)
     .setFont(font)
     .setAutoClear(false)
     .setText("" + process_y_)
     .setCaptionLabel("")
     .onChange(new CallbackListener() {
        public void controlEvent(CallbackEvent theEvent) {
          process_y_ = Integer.parseInt(process_y_txt_.getText());          
        }
      });
   
   cp5_.addButton("Save")
     .setValue(0)
     .setPosition(20, 145)
     .setSize(90,20)
     .onClick(new CallbackListener() {
       public void controlEvent(CallbackEvent theEvent) {
         selectOutput("Select a file to save:", "save_callback");
       }
     });
     
   cp5_.addButton("Load")
     .setValue(0)
     .setPosition(120, 145)
     .setSize(90,20)
     .onClick(new CallbackListener() {
       public void controlEvent(CallbackEvent theEvent) {
         selectInput("Select a file to load:", "load_callback");
       }
     });
     
   cp5_.addButton("Screenshot")
     .setValue(0)
     .setPosition(220, 145)
     .setSize(90,20)
     .onClick(new CallbackListener() {
       public void controlEvent(CallbackEvent theEvent) {
         noLoop();
         selectOutput("Select a file to save screenshot:", "screenshot_callback");
       }
     });     
   
   draw_lines_ = cp5_.addToggle("Draw lines")
     .setPosition(350, 145)
     .setSize(50,20)
     .setValue(false)
     .setColorLabel(1)
     .setMode(ControlP5.SWITCH);
   
   update_level_toggles();
   update_block_sliders();
   update_file_sliders();
}

void mousePressed() {
  int sample_x = (mouseX - (int)top_left_grid_.x + (grid_size / 2)) / grid_size;
  int sample_y = (mouseY - (int)top_left_grid_.y + (grid_size / 2)) / grid_size; 
  Vector v = new Vector(sample_x, sample_y);
  if (selected_samples_.contains(v)) {
    selected_samples_.remove(v);
  }
  else if (sample_x < dims_.x && sample_y < dims_.y) {
    selected_samples_.add(v);
  }
}

void save_callback(File selection)
{
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {    
    JSONObject json = new JSONObject();    
    json.setString("bit string", bit_string_);
    json.setInt("dims x", (int)dims_.x);
    json.setInt("dims y", (int)dims_.y);
    json.setInt("real dims x", real_dims_x_);
    json.setInt("real dims y", real_dims_y_);
    json.setInt("bits per block", bits_per_block_);
    json.setInt("blocks per file", blocks_per_file_);
    json.setInt("selected block", selected_block_);
    json.setInt("selected file", selected_file_);
    json.setInt("process x", process_x_);
    json.setInt("process y", process_y_);
    json.setInt("partition x", partition_x_);
    json.setInt("partition y", partition_y_);
    JSONArray level_array = new JSONArray();
    for (int i = 0; i < level_toggles_.size(); ++i) {
      Toggle t = level_toggles_.get(i);      
      level_array.setBoolean(i, t.getBooleanValue());
    }
    json.setJSONArray("level toggles", level_array);
    JSONArray selected_samples_array = new JSONArray();
    for (int i = 0; i < selected_samples_.size(); ++i) {
      Vector v = selected_samples_.get(i);      
      selected_samples_array.setString(i, Integer.toString((int)v.x) + " " + Integer.toString((int)v.y));
    }
    json.setJSONArray("selected samples", selected_samples_array);
    json.setBoolean("draw lines", draw_lines_.getBooleanValue());
    json.setBoolean("show processes", show_processes_.getBooleanValue());
    json.save(selection, selection.getName());    
  }
}

void load_callback(File selection)
{
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  }
  else {
    noLoop();
    JSONObject json = loadJSONObject(selection.getPath());
    bit_string_ = json.getString("bit string");
    dims_.x = json.getInt("dims x");
    dims_.y = json.getInt("dims y");
    real_dims_x_ = json.getInt("real dims x");  
    real_dims_y_ = json.getInt("real dims y");
    bits_per_block_ = json.getInt("bits per block");
    blocks_per_file_ = json.getInt("blocks per file");
    selected_block_ = json.getInt("selected block");
    selected_file_ = json.getInt("selected file");
      
    process_x_ = json.getInt("process x");
    process_y_ = json.getInt("process y");
    partition_x_ = json.getInt("partition x");
    partition_y_ = json.getInt("partition y");
    
    dims_x_txt_.setText(Integer.toString(real_dims_x_));
    dims_y_txt_.setText(Integer.toString(real_dims_y_));  
    bit_string_txt_.setText(bit_string_);    
        
    bits_per_block_txt_.setText(Integer.toString(bits_per_block_));
    blocks_per_file_txt_.setText(Integer.toString(blocks_per_file_));
    samples_per_block_ = (int)pow(2, bits_per_block_);
      
    partition_x_txt_.setText(Integer.toString(partition_x_));
    partition_y_txt_.setText(Integer.toString(partition_y_));
    process_x_txt_.setText(Integer.toString(process_x_));
    process_y_txt_.setText(Integer.toString(process_y_));
    
    JSONArray level_array = json.getJSONArray("level toggles");   
    for (int i = 0; i < level_array.size(); ++i) {    
      Toggle t = level_toggles_.get(i);
      t.setValue(level_array.getBoolean(i));
    }
    
    selected_samples_.clear();
    JSONArray selected_samples_array = json.getJSONArray("selected samples");
    for (int i = 0; i < selected_samples_array.size(); ++i) {
      String s = selected_samples_array.getString(i);
      String[] ss = splitTokens(s);
      selected_samples_.add(new Vector(Integer.parseInt(ss[0]), Integer.parseInt(ss[1])));
    }
        
    draw_lines_.setValue(json.getBoolean("draw lines"));
    show_processes_.setValue(json.getBoolean("show processes"));
    
    calculate_index_map(dims_, bit_string_);
    update_level_toggles();
    update_block_sliders();
    update_file_sliders();    
    loop();
  } //<>//
}

void screenshot_callback(File selection)
{
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  }
  else {        
    scale_ = 2;
    redraw();
    delay(2000);        
    int w = grid_size * (int)dims_.x + grid_size * 2;
    int h = grid_size * (int)dims_.y + grid_size * 2; 
    PImage slice = createImage(w*scale_, h*scale_, ARGB);
    int tlx = (int)top_left_grid_.x - grid_size * 3/2;
    int tly = (int)top_left_grid_.y - grid_size * 3/2;
    slice = pg.get(tlx*scale_, tly*scale_, w*scale_, h*scale_);
    slice.save(selection.getPath());
    scale_ = 1;
    loop();
  }
}

void update_block_sliders()
{
  int num_blocks = (int)dims_.x * (int)dims_.y / samples_per_block_;
  if (num_blocks < 1)
    num_blocks = 1;
  int backup = selected_block_;
  block_slider_.setRange(-1, num_blocks - 1);
  block_slider_.setNumberOfTickMarks(num_blocks + 1);
  block_slider_.setValue(backup);
}

void update_file_sliders()
{
  int num_files = (int)dims_.x * (int)dims_.y / samples_per_block_ / blocks_per_file_;
  if (num_files < 1)
    num_files = 1;
  int backup = selected_file_;
  file_slider_.setRange(-1, num_files - 1);
  file_slider_.setNumberOfTickMarks(num_files + 1);
  file_slider_.setValue(backup);
}

void update_level_toggles()
{
  int old_size = level_toggles_.size();
  int new_size = bit_string_.length() + 1;
  if (new_size > num_levels) {
    if (new_size > old_size) {
      for (int i = old_size; i < new_size; ++i) {
        level_toggles_.add(cp5_.addToggle("" + i)
          .setPosition(140 + i * 22, 55)
          .setSize(20,20)
          .setValue(true)
          .setLabelVisible(false));
      }
      for (int i = num_levels; i < old_size; ++i) {
        Toggle t = level_toggles_.get(i);
        t.setVisible(true);
      }
    }
    else { // make some toggles visible
      for (int i = num_levels; i < new_size; ++i) {
        Toggle t = level_toggles_.get(i);
        t.setVisible(true);
      }
    }
  }
  else { // disable the removed levels
    for (int i = new_size; i < num_levels; ++i) {
      Toggle t = level_toggles_.get(i);
      t.setVisible(false);
    }
  }
  num_levels = new_size;
}

void draw_grid()
{
  int samples_per_file = blocks_per_file_ * samples_per_block_;
  pg.stroke(100, 75, 50);
  pg.strokeWeight(1);
  pg.fill(240, 240, 240);
  PVector xy = new PVector(top_left_grid_.x, top_left_grid_.y);
  pg.rectMode(CENTER);
  for (int y = 0; y < dims_.y; ++y) {
    xy.x = top_left_grid_.x;
    for (int x = 0; x < dims_.x; ++x) {
      int s = y * (int)dims_.x + x;
      // check the selected block
      if (xy_to_hz_[s] / samples_per_block_ == selected_block_) {        
        pg.fill(227, 150, 227, 100);
        pg.stroke(150, 120, 227, 255);
        pg.strokeWeight(1);        
        pg.ellipse(xy.x, xy.y, grid_size + 5, grid_size + 5);        
      }
      else {
        pg.stroke(100, 75, 50, 255);
        pg.strokeWeight(1);
        pg.fill(240, 240, 240, 100);
      }
      // check the selected file
      if (xy_to_hz_[s] / samples_per_file == selected_file_) {
        pg.stroke(100, 75, 50, 255);
        pg.strokeWeight(1);
        pg.fill(130, 243, 227, 50);
        pg.pushMatrix();        
        pg.translate(xy.x, xy.y);
        pg.rotate(PI / 4);
        pg.rect(0, 0, grid_size-5, grid_size-5);
        pg.popMatrix();
      }
      
      if (x < real_dims_x_ && y < real_dims_y_) {        
        if (selected_samples_.contains(new Vector(x, y))) {
          pg.fill(180, 180, 180, 100);          
        }
        else {
          pg.fill(240, 240, 240, 100);          
        } 
        pg.rect(xy.x, xy.y, grid_size, grid_size);
      }
      xy.x += grid_size;
    }
    xy.y += grid_size;
  }
}

long interleave_bits(String bit_string, Vector coord)
{
  long interleaved_val = 0;
  final long one = 1;
  for (int i = bit_string.length() - 1; i >= 0; --i) {
    char v = bit_string.charAt(i);
    int j = bit_string.length() - i - 1;
    if (v == '0') {
      interleaved_val |= (coord.x & one) << j;
      coord.x >>= 1;
    }
    else if (v == '1') {
      interleaved_val |= (coord.y & one) << j;
      coord.y >>= 1;
    }
  }
  return interleaved_val;
}

Vector deinterleave_bits(String bit_string, long val)
{
    Vector coord = new Vector(0, 0);
    final long one = 1;
    for (int i = 0; i < bit_string.length(); ++i) {
        char v = bit_string.charAt(i);
        int j = bit_string.length() - i - 1;
        if (v == '0') {
            coord.x |= (val & (one << j)) >> j;
            coord.x <<= 1;
        }
        else if (v == '1') {
            coord.y |= (val & (one << j)) >> j;
            coord.y <<= 1;
        }
    }
    coord.x >>= 1;
    coord.y >>= 1;    
    return coord;
}

long z_to_hz(String bit_string, long z)
{
  final long one = 1;
  z |= one << (bit_string.length());
  z /= z & -z;
  z >>= 1;
  return z;
}

long xyz_to_hz(String bit_string, Vector coord)
{
  long z = interleave_bits(bit_string, coord);
  long hz = z_to_hz(bit_string, z);
  return hz;
}

int hz_to_level(long hz)
{
  int level = 0;  
  while (hz > 0) {
    hz /= 2;
    ++level;
  }
  return level;
}

int num_leading_zeros(long v)
{
  if (v == 0)
    return 64;
    
  int result = 0;
  while (v >= 0) {
      v = v << 1;
      ++result;
  }
  return result;
}

Vector hz_to_xy(String bit_string, long hz)
{    
    int leading_zeros = num_leading_zeros(hz);
    int z_level = leading_zeros - (64 - bit_string.length());    
    long z = hz << (z_level + 1);
    z |= (long)1 << z_level;
    Vector xy = deinterleave_bits(bit_string, z);
    return xy;
}

void calculate_index_map(PVector dims, String bit_string)
{
  xy_to_hz_ = new long[(int)(dims.x * dims.y)];
  xy_to_level_ = new int[xy_to_hz_.length];
  Vector v = new Vector(0, 0);
  for (int y = 0; y < dims.y; ++y) {
    for (int x = 0; x < dims.x; ++x) {
      v.x = x;
      v.y = y;
      long h = xyz_to_hz(bit_string, v);
      int s = y * (int)dims.x + x;
      xy_to_hz_[s] = h;
      xy_to_level_[s] = hz_to_level(h);
    }
  }
}

void draw_partitions()
{  
  int top = (int)top_left_grid_.y;
  int left = (int)top_left_grid_.x;
  pg.stroke(240, 20, 20, 120);
  pg.strokeWeight(4);
  if (partition_x_ > 0) {
    for (int i = 1; i < (real_dims_x_ + partition_x_ - 1)/ partition_x_; ++i) {
      pg.line(left + partition_x_ * grid_size * i - grid_size/2, top - grid_size - 10, left + partition_x_ * grid_size * i - grid_size/2, top + real_dims_y_ * grid_size + 10);
    }
  }
  
  if (partition_y_ > 0) {
    for (int i = 1; i < (real_dims_y_ + partition_y_ - 1)/ partition_y_; ++i) {
      pg.line(left - grid_size - 10, top + partition_y_ * grid_size * i - grid_size/2, left + real_dims_x_ * grid_size + 10, top + partition_y_ * grid_size * i - grid_size/2);
    }
  }     
}

void draw_processes()
{
  pg.textFont(font4_);
  int top = (int)top_left_grid_.y;
  int left = (int)top_left_grid_.x;
  pg.fill(20, 20, 240, 120);
  pg.stroke(20, 20, 240, 120);
  pg.strokeWeight(5);
  if (process_x_ > 0) {
    for (int i = 1; i < (real_dims_x_ + process_x_ - 1) / process_x_; ++i) {
      pg.line(left + process_x_ * grid_size * i - grid_size/2, top - grid_size - 10, left + process_x_ * grid_size * i - grid_size/2, top + real_dims_y_ * grid_size + 10);
    }
  }
  
  if (process_y_ > 0) {
    for (int i = 1; i < (real_dims_y_ + process_y_ - 1) / process_y_; ++i) {
      pg.line(left - grid_size - 10, top + process_y_ * grid_size * i - grid_size/2, left + real_dims_x_ * grid_size + 10, top + process_y_ * grid_size * i - grid_size/2);
    }
  }
  
  if (show_processes_.getBooleanValue() && (process_x_ > 0 || process_y_ > 0)) {
    int px = process_x_;
    int py = process_y_;
    if (px == 0)
      px = (int)real_dims_x_;
    if (py == 0)
      py = (int)real_dims_y_;
    pg.textSize(36);
    int pid = 0;
    for (int y = 0; y < (real_dims_y_ + py - 1) / py; ++y) {
      for (int x = 0; x < (real_dims_x_ + px - 1) / px; ++x) {        
        int z = (int)interleave_bits(bit_string_, new Vector((long)x, (long)y));        
        pg.text(pid, left + px * grid_size * x + (px * grid_size / 2) - grid_size/2, top + py * grid_size * y + (py * grid_size / 2) - grid_size/2);
        ++pid;
      }
    }    
  }
}

void draw_indices()
{  
  pg.textFont(font4_);
  pg.textSize(20);  
  pg.textAlign(CENTER, CENTER);
  Vector v = new Vector(0, 0);
  PVector xy = new PVector(top_left_grid_.x, top_left_grid_.y);  
  for (int y = 0; y < dims_.y; ++y) {
    xy.x = 100;
    for (int x = 0; x < dims_.x; ++x) {
      int s = x + y * (int)dims_.x;
      int l = xy_to_level_[s];
      Toggle t = level_toggles_.get(l);
      if (t.getBooleanValue()) {
        v.x = x;
        v.y = y;
        pg.fill(palette_[l % palette_.length]);               
        pg.text("" + xy_to_hz_[s], xy.x, xy.y);        
      }
      xy.x += grid_size;
    }
    xy.y += grid_size;
  }  
}

void draw_lines()
{
  if (!draw_lines_.getBooleanValue()) {
    return;
  }
     
  long dims = (long)(dims_.y * dims_.x);
  pg.fill(250, 50, 50, 50);
  pg.strokeWeight(2);
  float old_x = -1;
  float old_y = -1;
  for (long d = 0; d < dims; ++d) {
    Vector coord = hz_to_xy(bit_string_, d);
    int level = hz_to_level(d);
    Toggle t = level_toggles_.get(level);
    if (!t.getBooleanValue())
      continue;
      
    float x = top_left_grid_.x + coord.x * grid_size;
    float y = top_left_grid_.y + coord.y * grid_size;    
    if (old_x == -1) {
      old_x = x;
      old_y = y;
    }    
    pg.line(old_x, old_y, x, y);
    old_x = x;
    old_y = y;
  }
}

void draw()
{
  pg.beginDraw();
  pg.clear();
  pg.pushMatrix();
  pg.scale(scale_, scale_);
  pg.background(255, 255, 255);
  pg.fill(50, 75, 100);
  pg.textFont(font1_);
  pg.textSize(30);
  pg.textAlign(LEFT, BOTTOM);
  pg.text("Bit string: ", 20, 50);
  pg.text("Levels: ", 20, 80);
  pg.text("Dimensions: " + int(dims_.x) + " x " + int(dims_.y), 20, 140);
  pg.text("Real Dimensions: ", 20, 110);
  pg.text("x", 325, 110);
  pg.text("Bits per block: ", 450, 50);
  pg.text("Selected block: ", 450, 80);
  pg.text("Partition size: ", 450, 110);
  pg.text("Process size: ", 900, 110);
  pg.text("Blocks per file: ", 450, 140);
  pg.text("Selected file: ", 450, 170);
  pg.text("x", 735, 110);
  pg.text("x", 1155, 110);
  draw_grid();
  draw_indices();
  draw_processes();
  draw_partitions();
  draw_lines();
  pg.popMatrix();
  pg.endDraw();
  image(pg, 0, 0);
}